package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class UserProfileRepositoryTest extends JpaTestBase {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Test
    void should_get_and_save_composite_value() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            final UserProfile profile =
                userProfileRepository.save(new UserProfile(new UserName("Izumi", "Konata")));
            expectedId.setValue(profile.getId());
        });

        run(em -> {
            final UserProfile profile =
                userProfileRepository.findById(expectedId.getValue()).orElseThrow(
                    RuntimeException::new);

            assertEquals("Izumi", profile.getUserName().getFirstName());
            assertEquals("Konata", profile.getUserName().getLastName());
        });
    }

    @Test
    void should_update_composite_value() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            final UserProfile profile =
                userProfileRepository.save(new UserProfile(new UserName("Izumi", "Konata")));
            expectedId.setValue(profile.getId());
        });

        clear(em -> {
            // TODO
            //
            // 请书写一个测试，将姓名为 "Izumi, Konata" 的 profile 的姓名更新为 "John, Smith"。
            //
            // <--start-
            UserProfile userProfile = new UserProfile(new UserName("John", "Smith"));
            userProfile.setId(expectedId.getValue());
            userProfileRepository.saveAndFlush(userProfile);
            // --end->
        });

        run(em -> {
            final UserProfile userProfile = userProfileRepository
                .findById(expectedId.getValue()).orElseThrow(NoSuchElementException::new);
            assertEquals("John", userProfile.getUserName().getFirstName());
            assertEquals("Smith", userProfile.getUserName().getLastName());
        });
    }
}
